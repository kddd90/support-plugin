<?php
namespace Empu\Support\Behaviors;

use October\Rain\Extension\ExtensionBase;
use October\Rain\Support\Collection;

/**
 * Deprecated
 */
class HasValidationRules extends ExtensionBase
{
    public $validationRules = [];

    public function extendValidationRules(array &$rules): void
    {
        // modify $rules here
    }

    public function setValidationRules(array $validationRules)
    {
        $this->validationRules = $validationRules;
    }

    public function getValidationRules(string $context): Collection
    {
        $rules = $this->validationRules;
        $contextRules = new Collection();

        $this->extendValidationRules($rules);

        foreach ($rules as $fieldKey => $rules) {
            if (! $this->isLineInContext($context, $fieldKey)) {
                continue;
            }

            $contextRules->put($fieldKey, $rules);
        }

        return $contextRules;
    }

    public function getFieldValidationRule(string $context, string $field): array
    {
        $rules = $this->getValidationRules($context);

        return $rules[$field] ?? [];
    }

    protected function isLineInContext(string $context, string &$fieldKey): bool
    {
        $pattern = '~(?P<field>[0-9a-zA-Z\-_]+)(@(?P<context>create|retrieve|update|delete))?~';
        
        if (! preg_match($pattern, $fieldKey, $match)) {
            return false;
        }
        
        $fieldKey = $match['field'];

        return ! isset($match['context']) || $match['context'] == $context;
    }
    
}
