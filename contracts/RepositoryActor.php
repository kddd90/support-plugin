<?php

namespace Empu\Support\Contracts;

interface RepositoryActor
{
    /**
     * Get the registered repo alias for the model.
     *
     * @return string
     */
    public function getRepositoryAlias(): string;
}
