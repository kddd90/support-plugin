<?php
namespace Empu\Support\Contracts;

interface Repository
{
    public function find($identifier);

    public function getAll(array $columns = ['*']);

    public function getPaginated(?int $pageSize, array $columns = ['*']);

    public function create(array $payload);

    public function update(array $payload);

    public function delete($identifier);
}
