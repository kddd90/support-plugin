<?php
namespace Empu\Support\Transformers;

use League\Fractal\TransformerAbstract;
use System\Models\File;

class FileTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(File $file)
    {
        return [
            'id' => $file->uuid,
            'url' => $file->getPath(),
            'thumbs' => $file->isImage() ? $this->makeThumbs($file) : null,
            'file_name' => $file->file_name,
            'file_size' => (float)$file->file_size,
            'content_type' => $file->content_type,
            'title' => $file->title ?? '',
            'description' => $file->description ?? '',
            'created_at' => $file->created_at->toW3cString(),
            'updated_at' => $file->updated_at->toW3cString(),
        ];
    }

    protected function makeThumbs(File $file)
    {
        if (! $file->isImage()) {
            return null;
        }
        
        $crop = ['mode' => 'crop'];

        return [
            'square' => $file->getThumb(75, 75, $crop),
            'small' => $file->getThumb(180, 240, $crop),
            'medium' => $file->getThumb(375, 500, $crop),
        ];
    }
}
