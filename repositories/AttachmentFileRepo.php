<?php
namespace Empu\Support\Repositories;

use Empu\Support\Models\AttachmentFile;
use Empu\Support\Contracts\RepoInterface;
use Empu\Support\Repository\RepoBase;
use October\Rain\Database\Collection;

class AttachmentFileRepo extends RepoBase implements RepoInterface
{
    public $identifierColumn = 'uuid';

    public function __construct()
    {
        $this->setModel(new AttachmentFile);
    }
}
