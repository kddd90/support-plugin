<?php
namespace Empu\Support\Models;

use System\Models\File;
use Empu\Support\Behaviors\WithRefTrait;

class AttachmentFile extends File
{
    use WithRefTrait;

    public $refKey = 'uuid';

    public function beforeCreate()
    {
        $this->generateRef();

        parent::beforeCreate();
    }
}
